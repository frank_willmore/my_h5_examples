#define _GNU_SOURCE

#include <stdio.h>
#include <mpi.h>

#include <dlfcn.h>
 
// typedef int (*orig_open_f_type)(const char *pathname, int flags);
typedef int (*orig_mpi_init_f_type)(int *argc, char ***argv);
typedef int (*orig_mpi_finalize_f_type)();
 
int MPI_Init(int *argc, char ***argv) 
{
    /* Some evil injected code goes here. */
    printf("My mpi_init says hi\n");
     
    orig_mpi_init_f_type orig_mpi_init;
    orig_mpi_init = (orig_mpi_init_f_type)dlsym(RTLD_NEXT,"MPI_Init");
    return orig_mpi_init(argc, argv);
}


int MPI_Finalize(void) 
{
    /* Some evil injected code goes here. */
    printf("My mpi_finalize says hi\n");
     
    orig_mpi_finalize_f_type orig_mpi_finalize;
    orig_mpi_finalize = (orig_mpi_finalize_f_type)dlsym(RTLD_NEXT,"MPI_Finalize");
    return orig_mpi_finalize();
}



