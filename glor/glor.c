#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>

// Glor is a she-bang interpreter template... 
// it can be placed at the top of a file, and then interpret the following lines.

int main(int argc, char *argv[]){

    printf("Glor 0.0\t(C) 2016 Frank Willmore\n\n");

    for (int i=0; i<argc; i++) printf("argv[%d] == %s\n", i, argv[i]);

    FILE *in = stdin;

    // called on the command line
    if (argc==1) printf("Why are you calling me from the command line?\n");

    // called as interpreter
    else if (argc == 2) 
    {
        printf("Opening %s for interpretation.\n", argv[argc-1]);
        in = fopen(argv[argc-1], "r");
    }

    char *line = NULL; size_t n = 0; printf("! "); 
    while(getline(&line, &n, in) != EOF) {
        printf("! "); // just a prompt

        // printf("! got %s", line);

        // getline allocates mem, so we need to free it and reset params for the next call
        free(line); line = NULL; n=0;
    }
    

}

