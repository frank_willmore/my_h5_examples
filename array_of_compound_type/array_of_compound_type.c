// array_of_compound_type.c 
#include "hdf5.h"
#include <stdlib.h>

typedef struct {
	double A;
   double B;
}  my_struct;

int main(void){

	my_struct data[5];
	hsize_t dims[] = {5};

	// add some data
	for (int i=0;i<5; i++) data[i].A = (2.0 * (data[i].B = (double)(i)));

	hid_t file = H5Fcreate("array.h5", H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

	// create the compound type
	hid_t compound_t = H5Tcreate(H5T_COMPOUND, sizeof(my_struct)); 
	H5Tinsert(compound_t, "part A", HOFFSET(my_struct, A), H5T_NATIVE_DOUBLE);
	H5Tinsert(compound_t, "part B", HOFFSET(my_struct, B), H5T_NATIVE_DOUBLE);

	hid_t file_h5t = H5Tarray_create(compound_t, 1, dims); 
	hid_t mem_h5t  = H5Tarray_create(compound_t, 1, dims); 

	// create dataspace
	hid_t dataspace = H5Screate_simple(1, dims, NULL);
	hid_t dataset   = H5Dcreate(file, "my_dataset", compound_t, dataspace, H5P_DEFAULT, H5P_DEFAULT,H5P_DEFAULT); 
	
	H5Dwrite(dataset, compound_t, H5S_ALL, H5S_ALL, H5P_DEFAULT, data);

	H5Fclose(file);
	H5Tclose(compound_t);

}
