#include "hdf5.h"
#include <stdlib.h>
#include <assert.h>

#define FILE            "H5Tarray_create_example.h5"
#define DATASET         "DS1"
#define ATTRIBUTE       "A1"
#define DIM0            4
#define ADIM0           3
#define ADIM1           5

int main (void) {
    hsize_t     dims[1] = {DIM0},
                adims[2] = {ADIM0, ADIM1};
    int         wdata[DIM0][ADIM0][ADIM1],      /* Write buffer */
                ndims,
                i, j, k;

    /* * Initialize data.  i is the element in the dataspace, j and k the * elements within the array datatype.  */
    for (i=0; i<DIM0; i++) for (j=0; j<ADIM0; j++) for (k=0; k<ADIM1; k++) wdata[i][j][k] = i * j - j * k + i * k;

    /* * Create a new file using the default properties.  */
    hid_t file = H5Fcreate (FILE, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

    /* * Create array datatypes for file and memory.  */
    hid_t filetype = H5Tarray_create (H5T_STD_I64LE, 2, adims);
    hid_t memtype = H5Tarray_create (H5T_NATIVE_INT, 2, adims);

	 // create dataspace
	 hid_t dataspace = H5Screate_simple (1, dims, NULL);
	 hid_t dataset = H5Dcreate(file, "my_dataset", memtype, dataspace, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

	 assert(!H5Dwrite(dataset, memtype, H5S_ALL, H5S_ALL, H5P_DEFAULT, wdata));

    /* * Close and release resources.  */
    H5Sclose (dataspace);
    H5Tclose (filetype);
    H5Tclose (memtype);
    H5Fclose (file);

    return 0;
}
