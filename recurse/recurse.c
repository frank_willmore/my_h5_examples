#include "hdf5.h"
#include <stdlib.h>

// forward declaration?

typedef struct {
    void     *inner;
}   my_t;

int main (void) {

    hsize_t dataspace_dims[] = {1}, array_dims[] = {5};

    // create a compound type to represent my_t
    hid_t my_h5t = H5Tcreate(H5T_COMPOUND, sizeof(my_t));
    H5Tinsert(my_h5t, "Inner", HOFFSET(my_t, inner), H5T_NATIVE_LONG);

    // wrap the type with an array
    hid_t my_h5at = H5Tarray_create(my_h5t, 1, array_dims);
    // create the data which will be held, default to NULL pointers
    my_t data[] = {NULL, NULL, NULL, NULL, NULL};

    /* Create a new file using the default properties.  */
    hid_t file = H5Fcreate ("recurse.h5", H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

	// create dataspace, dataset in that file
	hid_t dataspace = H5Screate_simple (1, dataspace_dims, NULL);
	hid_t dataset = H5Dcreate(file, "my_dataset", my_h5at, dataspace, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

	// H5Dwrite(dataset, memtype, H5S_ALL, H5S_ALL, H5P_DEFAULT, wdata[0][0]);
	H5Dwrite(dataset, my_h5at, H5S_ALL, H5S_ALL, H5P_DEFAULT, data);

    /* * Close and release resources.  */
    H5Sclose (dataspace);
    H5Tclose (my_h5at);
    H5Tclose (my_h5t);
    H5Fclose (file);

    return 0;
}
