// read

#include <hdf5.h> 
#include <stdio.h> 
#include <assert.h>

typedef struct {
    char *name;
    unsigned int dims[3];
}  info_t;

void main(int argc, char *argv[]){

    // open the file
    hid_t fapl_id = H5P_DEFAULT;
    hid_t file = H5Fopen( "attr.h5", H5F_ACC_RDONLY, fapl_id );

    // create the string type
    hid_t vls_type_c_id = H5Tcopy(H5T_C_S1);
    H5Tset_size(vls_type_c_id, H5T_VARIABLE);

    // create the array type for dims
    hsize_t adims[] = {3};
    hid_t mem_h5t = H5Tarray_create(H5T_NATIVE_UINT, 1, adims);

	// create the compound type
	hid_t compound_t = H5Tcreate(H5T_COMPOUND, sizeof(info_t)); 
	H5Tinsert(compound_t, "dimensions", HOFFSET(info_t, dims), mem_h5t );
	H5Tinsert(compound_t, "the name", HOFFSET(info_t, name), vls_type_c_id );

    // create the dataspace
    // hid_t space_id = H5Screate(H5S_SCALAR);

    // open the attribute, and read info into a buffer
    hid_t aapl_id = H5P_DEFAULT;
    hid_t lapl_id = H5P_DEFAULT;
    hid_t attr_id = H5Aopen_by_name( file, "/", "my_attr", aapl_id, lapl_id );
    assert(attr_id >= 0);
    info_t buf;
    assert( H5Aread(attr_id, compound_t, &buf ) >= 0);

    int i;
    for (i=0;i<3;i++) printf("%d\n", buf.dims[i]);
    printf("\nname=%s\n", buf.name);

    H5Aclose(attr_id);
	H5Tclose(compound_t);
	H5Fclose(file);
}

