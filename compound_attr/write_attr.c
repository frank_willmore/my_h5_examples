#include <hdf5.h> 
#include <stdio.h> 
#include <assert.h>
#include <stdlib.h>

typedef struct {
    char *name;
    unsigned int dims[3];
}  info_t;

int main(int argc, char *argv[]){

    hid_t fcpl_id = H5P_DEFAULT; 
    hid_t fapl_id = H5P_DEFAULT;
    hid_t file = H5Fcreate( "attr.h5", H5F_ACC_TRUNC, fcpl_id, fapl_id );

    // create the string type
    hid_t vls_type_c_id = H5Tcopy(H5T_C_S1);
    H5Tset_size(vls_type_c_id, H5T_VARIABLE);

    // create the array type for dims
    hsize_t adims[] = {3};
    hid_t mem_h5t = H5Tarray_create(H5T_NATIVE_UINT, 1, adims);

	// create the compound type
	hid_t compound_t = H5Tcreate(H5T_COMPOUND, sizeof(info_t)); 
	H5Tinsert(compound_t, "dimensions", HOFFSET(info_t, dims), mem_h5t );
	H5Tinsert(compound_t, "the name", HOFFSET(info_t, name), vls_type_c_id );

    hid_t space_id = H5Screate(H5S_SCALAR);
    hid_t acpl_id = H5P_DEFAULT;
    hid_t aapl_id = H5P_DEFAULT;

    hid_t attr_id = H5Acreate2( file, "my_attr", compound_t, space_id, acpl_id, aapl_id );
    info_t buf;
    buf.name = "my_name";
    buf.dims[0] = 2;
    buf.dims[1] = 3;
    buf.dims[2] = 4;
    assert(H5Awrite(attr_id, compound_t, &buf ) >= 0);

    H5Aclose(attr_id);
	H5Tclose(compound_t);
	H5Fclose(file);
}



