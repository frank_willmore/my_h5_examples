#include "H5Zzfp.h"

#include "hdf5.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

void main(int argc, char** argv)
{
  float data[1024][2048];
  size_t i, j, cd_nelmts = 4;
  unsigned int cd_values[] = {3, 0, 0, 0};
  hid_t fapl, file, dcpl, fspace, dset;
  hsize_t dims[2] = { 1024, 2048 }, cdims[2] = { 256, 512 };

  /* initialize test data */

  for (i = 0; i < 1024; ++i)
    {
      for (j = 0; j < 2048; ++j)
        {
          data[i][j] = (float) i + j;
        }
    }

  /* use the latest file format */

  fapl = H5Pcreate(H5P_FILE_ACCESS);
  assert(fapl >= 0);
  assert(H5Pset_libver_bounds(fapl, H5F_LIBVER_LATEST, H5F_LIBVER_LATEST) >= 0);

  file = H5Fcreate("zfp_sample.h5", H5F_ACC_TRUNC, H5P_DEFAULT, fapl);
  assert(file >= 0);

  fspace = H5Screate_simple(2, dims, dims);
  assert(fspace >= 0);

  /* initialize the ZFP library */

  assert(H5Z_zfp_initialize() >= 0);

  dcpl = H5Pcreate(H5P_DATASET_CREATE);
  assert(dcpl >= 0);

  assert(H5Pset_chunk(dcpl, 2, cdims) >= 0);

  /* set ZFP compression */

  assert(H5Pset_filter(dcpl, H5Z_FILTER_ZFP, H5Z_FLAG_MANDATORY,cd_nelmts,
                       cd_values) >= 0);

  dset = H5Dcreate2(file, "data", H5T_IEEE_F32LE, fspace,
                    H5P_DEFAULT, dcpl, H5P_DEFAULT);
  assert(dset >= 0);

  assert(H5Dwrite(dset, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, H5P_DEFAULT,
                  data) >= 0);

  /* check the compression ratio */

  printf("Uncompressed size\t :%d bytes\n", 1024*2048*sizeof(float));
  printf("Compressed size\t\t :%d bytes\n", H5Dget_storage_size(dset));

  /* clean house */

  assert(H5Dclose(dset) >= 0);
  assert(H5Pclose(dcpl) >= 0);
  assert(H5Sclose(fspace) >= 0);
  assert(H5Fclose(file) >= 0);
  assert(H5Pclose(fapl) >= 0);

  assert(H5Z_zfp_finalize() >= 0);
}
