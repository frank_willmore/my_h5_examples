// array_of_nested.c

#include "hdf5.h"
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#define DATASET_DIM 5

#define FILE            "array_of_nested.h5"
#define DATASET         "DS1"
#define ATTRIBUTE       "A1"
#define H5ARRAY_DIM     3
#define ADIM0           4
#define ADIM1           5

typedef struct {
	int      inner_id;
	char*    inner_string;
} inner_t;

typedef struct {
	int      outer_id;
	inner_t  contained_inner;
} outer_t;

int main (void) {
    hsize_t     h5_dims[] = {H5ARRAY_DIM};
                //adims[2] = {ADIM0, ADIM1};
                adims[1] = {ADIM0};
    int         //wdata[DIM0][ADIM0][ADIM1],      /* Write buffer */
                ndims,
                i, j, k;

    // create the contained inner string type
    hid_t inner_string_h5t = H5Tcopy(H5T_C_S1);
    H5Tset_size(inner_string_h5t, H5T_VARIABLE);
    // create the inner compound type 
    hid_t inner_t_h5t = H5Tcreate(H5T_COMPOUND, sizeof(inner_t)); 
    // insert the inner contained types into the inner compound type
    assert(!H5Tinsert(inner_t_h5t, "Inner ID", HOFFSET(inner_t, inner_id), H5T_NATIVE_INT));
    assert(!H5Tinsert(inner_t_h5t, "Inner String", HOFFSET(inner_t, inner_string), inner_string_h5t));

	// create the outer compound type and insert its contained types
	hid_t outer_t_h5t = H5Tcreate(H5T_COMPOUND, sizeof(outer_t)); 
    // insert the outer contained types
    assert(!H5Tinsert(outer_t_h5t, "Outer ID", HOFFSET(outer_t, outer_id), H5T_NATIVE_INT));
    assert(!H5Tinsert(outer_t_h5t, "Contained Inner", HOFFSET(outer_t, contained_inner), inner_t_h5t));

    /* * Create array datatypes for file and memory.  */
    hid_t filetype = H5Tarray_create (outer_t_h5t, 1, adims);
    hid_t memtype = H5Tarray_create (outer_t_h5t, 1, adims);
    
    hid_t file = H5Fcreate (FILE, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT); // Create a new file using the default properties. 
    hid_t dataspace = H5Screate_simple (1, dims, NULL);                     // create dataspace
    hid_t dataset = H5Dcreate(file, "my_dataset", memtype, dataspace, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

    assert(!H5Dwrite(dataset, memtype, H5S_ALL, H5S_ALL, H5P_DEFAULT, wdata[0][0]));

	// make some data to store...
	outer_t the_data[DATASET_DIM];
	int i;
	for (i=0; i<DATASET_DIM; i++) {
		the_data[i].outer_id=i;
                // now create some junk to stash in a new string
		char *junk = (char*) malloc(15+i);
		memset (junk, 65+i, 15+i); // 15+i ascii chars of val 65+i
		junk[15+i] = 0; // null terminator for string
		the_data[i].contained_inner.inner_string = junk;
		the_data[i].contained_inner.inner_id = 100 + i;	
	}

	// create the file
	char *filename;
	file = H5Fcreate(filename = "nc.h5", H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

	// open dataset
	hsize_t dims[] = {DATASET_DIM};
	dataspace = H5Screate_simple (1, dims, NULL); 
	dataset = H5Dcreate(file, "my_dataset", outer_t_h5t, dataspace, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);   

	// write it out
	assert(!H5Dwrite(dataset, outer_t_h5t, H5S_ALL, H5S_ALL, H5P_DEFAULT, the_data));   

	// close resources
	H5Fclose(file);
	H5Tclose(inner_string_h5t);
	H5Tclose(inner_t_h5t);
	H5Tclose(outer_t_h5t);
	
	// free the memory
	for (i=0; i<DATASET_DIM; i++) free(the_data[i].contained_inner.inner_string); 

    /* * Close and release resources.  */
    H5Sclose (dataspace);
    H5Tclose (filetype);
    H5Tclose (memtype);
    H5Fclose (file);

}

