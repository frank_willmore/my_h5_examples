// dataspace_of_arrays.c

#include "hdf5.h"
#include <stdlib.h>
#include <string.h>
#include <assert.h>

int main(void){

	hsize_t dims[] = {3}; // dimensions of array, will be rank 1
	int some_data[] = {24,33,44};

	hid_t file = H5Fcreate("dso3dra.h5", H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

	hid_t array_of_int_h5t = H5Tarray_create(H5T_NATIVE_INT, 1, dims);
	hsize_t m_dims[] = {1}; // dimensions of array, will be rank 1
	hid_t memspace = H5Screate_simple(1, m_dims, NULL);
	hid_t filespace = H5Screate_simple(1, dims, NULL);
    hid_t dataset = H5Dcreate(file, "my_data", array_of_int_h5t, filespace, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

    hsize_t n_coords = 1;
    hsize_t coords[] = {0};
    H5Sselect_elements( filespace, H5S_SELECT_SET, 1, coords );
    H5Dwrite(dataset, array_of_int_h5t, memspace, filespace, H5P_DEFAULT, some_data);

    coords[0] = 2;
    H5Sselect_elements( filespace, H5S_SELECT_SET, 1, coords );
	H5Dwrite(dataset, array_of_int_h5t, memspace, filespace, H5P_DEFAULT, some_data);

	H5Dclose(dataset);
	H5Sclose(filespace);
	H5Sclose(memspace);
	H5Tclose(array_of_int_h5t);
	H5Fclose(file);

	return 0;
}
