// array_of_int.c

#include "hdf5.h"
#include <stdlib.h>
#include <assert.h>

int main(void){

	hsize_t dims[] = {3}; // dimensions of array, will be rank 1
	hsize_t adims[] = {1}; // dimensions of ???

	int some_data[] = {24,33,44};

	hid_t file = H5Fcreate("aoint.h5", H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

	hid_t mem_h5t = H5Tarray_create(H5T_NATIVE_INT, 1, adims);

	hid_t dataspace = H5Screate_simple(1, dims, NULL);

   hid_t dataset = H5Dcreate(file, "my_data", mem_h5t, dataspace, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

	H5Dwrite(dataset, mem_h5t, H5S_ALL, H5S_ALL, H5P_DEFAULT, some_data);

	H5Dclose(dataset);
	H5Sclose(dataspace);
	H5Tclose(mem_h5t);
	H5Fclose(file);

	return 0;
}
