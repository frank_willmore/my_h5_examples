// array_of_variable_length_str.c

#include "hdf5.h"
#include <stdlib.h>
#include <string.h>
#include <assert.h>

int main(void){

	hsize_t array_dims[] = {3}; // dimensions of array, will be rank 1
	//char* data[3];

	char* junk_data[] = {"this", "is", "junk"};

	hid_t file = H5Fcreate("aovls.h5", H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

	// create the types
	hid_t vlstr_t = H5Tcopy(H5T_C_S1);
    H5Tset_size(vlstr_t, H5T_VARIABLE);
	hid_t aovls_t = H5Tarray_create(vlstr_t, 1, array_dims);

    // create the dataspace
    hsize_t f_dims[] = {1};
	hid_t space_id = H5Screate_simple(1, f_dims, NULL);

    // create the attribute
    hid_t attr_id = H5Acreate(file, "attr_name", aovls_t, space_id, H5P_DEFAULT, H5P_DEFAULT); 

	//H5Dwrite(dataset, aovls_t, mem_space, file_space, H5P_DEFAULT, junk_data);

	H5Awrite(attr_id, aovls_t, junk_data);

	H5Aclose(attr_id);
	H5Sclose(space_id);
	H5Tclose(aovls_t);
	H5Tclose(vlstr_t);
	H5Fclose(file);

	return 0;
}
