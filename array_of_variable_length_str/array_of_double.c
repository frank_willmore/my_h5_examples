// array_of_double.c

#include "hdf5.h"
#include <stdlib.h>

int main(void){

	hsize_t dims_of_h5t_array[] = {2,3};
	hsize_t dims_of_c_array[] = {1};

	double some_data[2][3] = {{1.0, 2.0, 3.0}, {4.0, 5.0, 6.0}}; // 2 rows, 3 cols

	hid_t file = H5Fcreate("aod.h5", H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
	hid_t mem_h5t = H5Tarray_create(H5T_NATIVE_DOUBLE, 2, dims_of_h5t_array);
	hid_t dataspace = H5Screate_simple(1, dims_of_c_array, NULL);
	hid_t dataset = H5Dcreate(file, "my_data", mem_h5t, dataspace, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	H5Dwrite(dataset, mem_h5t, H5S_ALL, H5S_ALL, H5P_DEFAULT, some_data);

	H5Dclose(dataset);
	H5Sclose(dataspace);
	H5Tclose(mem_h5t);
	H5Fclose(file);

}
