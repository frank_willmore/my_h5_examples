// 2d_array_of_int.c

#include "hdf5.h"
#include <stdlib.h>

int main(void){

	hsize_t dims_of_h5t_array[] = {2,3};
	hsize_t dims_of_dataspace[] = {4};

	int *data = (int *)malloc(24 * sizeof(int));
	int i; for (i=0; i<24; i++) data[i] = i;

	hid_t file = H5Fcreate("aoi.h5", H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
	hid_t mem_h5t = H5Tarray_create(H5T_NATIVE_INT, 2, dims_of_h5t_array);
	hid_t dataspace = H5Screate_simple(1, dims_of_dataspace, NULL);
	hid_t dataset = H5Dcreate(file, "the_data", mem_h5t, dataspace, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	H5Dwrite(dataset, mem_h5t, H5S_ALL, H5S_ALL, H5P_DEFAULT, data); 

	return 0;
}
