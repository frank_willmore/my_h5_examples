
#include "hdf5.h"
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <iostream>

using namespace std;

class BigDeal
{
    public:

        int argc;
        int *argi; // array of ints

};

int main(void){

    hid_t file = H5Fcreate("aovls.h5", H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

	// create the vls type and array of vls type
    //hid_t vlstr_t = H5Tcopy(H5T_C_S1);
    //H5Tset_size(vlstr_t, H5T_VARIABLE);
    hsize_t array_dims[1]; // dimensions of array, will be rank 1
    array_dims[0] = 2; // argc
    //hid_t aovls_t = H5Tarray_create(vlstr_t, 1, array_dims);
    hid_t aoi_t = H5Tarray_create(H5T_NATIVE_INT, 1, array_dims);

    // create the compound type
    hid_t compound_t = H5Tcreate(H5T_COMPOUND, sizeof(BigDeal)); 
    assert(H5Tinsert(compound_t, "argc", HOFFSET(BigDeal, argc), H5T_NATIVE_INT) >= 0);
    //assert(H5Tinsert(compound_t, "argv", HOFFSET(BigDeal, argv), aovls_t) >= 0);
    assert(H5Tinsert(compound_t, "argi", HOFFSET(BigDeal, argi), aoi_t) >= 0);

    // create the dataspace
    hsize_t space_dims[] = {1};
    hid_t space_id = H5Screate_simple(1, space_dims, NULL);

    // create the compound attribute
    BigDeal bd;
    bd.argc = 2;
    //char* thisthat[] = {"this", "that"};
    //bd.argv = thisthat;
    int some_ints[2];
    some_ints[0] = 3;
    some_ints[1] = 5;
    bd.argi = some_ints; // = some_ints;

    hid_t big_attr_id = H5Acreate(file, "big_attr", compound_t, space_id, H5P_DEFAULT, H5P_DEFAULT);
    assert(big_attr_id >= 0);
    assert(H5Awrite(big_attr_id, compound_t, bd) >= 0);

    /*
    hid_t attr_id = H5Acreate(file, "big_attr", aoi_t, space_id, H5P_DEFAULT, H5P_DEFAULT);
    assert(attr_id >= 0);
    assert(H5Awrite(attr_id, aoi_t, some_ints) >= 0);
    */

	H5Aclose(big_attr_id);
	H5Sclose(space_id);
	//H5Tclose(aovls_t);
	//H5Tclose(vlstr_t);
	H5Tclose(aoi_t);
	H5Fclose(file);

	return 0;
}
