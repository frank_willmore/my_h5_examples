/**
 * nc.c -- example which creates a nested compound type, of which the inner type contains 
 * a variable length string element. Note that this simple example does not generate a 
 * separate file representation to control for variance between memory representation 
 * on generating machine and consuming machine.
 *
 * Frank Willmore (frank.willmore@hdfgroup.org) October 13, 2016
 */

#include "hdf5.h"
#include <string.h>
#include <stdlib.h>
#include <assert.h>

#define DATASET_DIM 5

typedef struct inner_t {
	int		inner_id;
	char* 	inner_string;
} inner_t;

typedef struct outer_t {
	int      outer_id;
	inner_t  contained_inner;
} outer_t;

int main(void){

	hid_t file, dataset, dataspace;
	hid_t inner_compound_type, outer_compound_type;

	// make some data to store...
	outer_t the_data[DATASET_DIM];
	int i;
	for (i=0; i<DATASET_DIM; i++) {
		the_data[i].outer_id=i;
        // now create some junk to stash in a new string
        char *junk = (char*) malloc(15+i);
        memset (junk, 65+i, 15+i); // 15+i ascii chars of val 65+i
        junk[15+i] = 0; // null terminator for string
        the_data[i].contained_inner.inner_string = junk;
        the_data[i].contained_inner.inner_id = 100 + i;	
	}
	
	// create the inner compound type 
	hid_t inner_t_h5t = H5Tcreate(H5T_COMPOUND, sizeof(inner_t)); 

	// create the contained inner string type
	hid_t inner_string_h5t = H5Tcopy(H5T_C_S1);
	H5Tset_size(inner_string_h5t, H5T_VARIABLE);

	// insert the inner contained types
	assert(!H5Tinsert(inner_t_h5t, "Inner ID", HOFFSET(inner_t, inner_id), H5T_NATIVE_INT));
	assert(!H5Tinsert(inner_t_h5t, "Inner String", HOFFSET(inner_t, inner_string), inner_string_h5t));

	// create the outer compound type and insert its contained types
	hid_t outer_t_h5t = H5Tcreate(H5T_COMPOUND, sizeof(outer_t)); 

	// insert the outer contained types
	assert(!H5Tinsert(outer_t_h5t, "Outer ID", HOFFSET(outer_t, outer_id), H5T_NATIVE_INT));
   assert(!H5Tinsert(outer_t_h5t, "Contained Inner", HOFFSET(outer_t, contained_inner), inner_t_h5t));

	// create the file
	char *filename;
	file = H5Fcreate(filename = "nc.h5", H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

	// open dataset
	hsize_t dims[] = {DATASET_DIM};
	dataspace = H5Screate_simple (1, dims, NULL); 
	dataset = H5Dcreate(file, "my_dataset", outer_t_h5t, dataspace, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);   

	// write it out
	assert(!H5Dwrite(dataset, outer_t_h5t, H5S_ALL, H5S_ALL, H5P_DEFAULT, the_data));   

	// close resources
	H5Fclose(file);
	H5Tclose(inner_string_h5t);
	H5Tclose(inner_t_h5t);
	H5Tclose(outer_t_h5t);
	
	// free the memory
	for (i=0; i<DATASET_DIM; i++) free(the_data[i].contained_inner.inner_string); 

}

