#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>

#include "hdf5.h"

int main(int argc, char *argv[]) {

    hsize_t n_dims = 2;
    hsize_t dims[] = {1024, 1024};

    hid_t file = H5Fcreate("my_file.h5", H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
    assert (file >= 0);
    hid_t space = H5Screate_simple(n_dims, dims, NULL);
    assert (space >= 0);
    hid_t set = H5Dcreate(file, "my_set", H5T_NATIVE_INT, space, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    assert (set >= 0);
    
    int *data = (int *)malloc(1024*1024*sizeof(int)); 

    assert ( H5Dwrite(set, H5T_NATIVE_INT, space, space, H5P_DEFAULT, data) >= 0);

    free(data);

    assert ( H5Dclose(set) >= 0);
    assert ( H5Sclose(space) >= 0);
    assert ( H5Fclose(file) >= 0);

}

