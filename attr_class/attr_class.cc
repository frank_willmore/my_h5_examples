#include <iostream>

class attrClass
{

    public: 

    int ID;

    static int lastID; 

    static int getNextID()
    {
        return lastID++;
    }

    attrClass()
    {
        ID = getNextID();
        std::cout << "creating ID=" << ID << std::endl;
    }

    attrClass(int init)
    {
        ID = init;
        std::cout << "created with specified ID=" << ID << std::endl;
    }

    int getID()
    {
        return ID;
    }

    void sayHello()
    {
        std::cout << "hello from #" << ID << std::endl;
    }

    ~attrClass()
    {
        std::cout << "deleting #" << ID << std::endl;
    }

};

int attrClass::lastID=0;

int main(int argc, char *argv[])
{
    attrClass my_attr2=attrClass();
    attrClass ac(5);
    attrClass my_attr=attrClass();

    std::cout << "got: " << my_attr.getID() << std::endl;
    std::cout << "got ac: " << ac.getID() << std::endl;
//    std::cout << "class: " << my_attr << std::endl;
//    std::cout << "class3: " << my_attr3 << std::endl;
    ac.sayHello();
    my_attr.sayHello();

    attrClass *heap = new attrClass(29);
    heap->sayHello();

    attrClass *heap2 = new attrClass();
    heap2->sayHello();

    delete(heap);
    delete(heap2);
}

