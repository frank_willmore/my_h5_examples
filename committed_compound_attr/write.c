#include <hdf5.h>
#include <assert.h>
#include <stdio.h> 
#include <stdlib.h>

typedef struct {
    char *name;
    unsigned int dims[3];
}  info_t;

int main(int argc, char *argv[]){

    hid_t file = H5Fcreate("ct.h5", H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
    assert(file >= 0);

    // create the string type
    hid_t vls_type_c_id = H5Tcopy(H5T_C_S1);
    H5Tset_size(vls_type_c_id, H5T_VARIABLE);

    // create the array type for dims
    hsize_t adims[] = {3};
    hid_t mem_h5t = H5Tarray_create(H5T_NATIVE_UINT, 1, adims);

	// create the compound type
	hid_t compound_t = H5Tcreate(H5T_COMPOUND, sizeof(info_t)); 
	H5Tinsert(compound_t, "dimensions", HOFFSET(info_t, dims), mem_h5t );
	H5Tinsert(compound_t, "the name", HOFFSET(info_t, name), vls_type_c_id );

    // commit the compound type
    assert( H5Tcommit( file, "my_cool_type", compound_t, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT ) >= 0 );

    // create the dataspace and attribute
    hid_t space_id = H5Screate(H5S_SCALAR);
    hid_t acpl_id = H5P_DEFAULT;
    hid_t aapl_id = H5P_DEFAULT;
    hid_t attr_id = H5Acreate( file, "my_attr", compound_t, space_id, acpl_id, aapl_id );

    // create a buffer, assign values, and write
    info_t buf;
    buf.name = "my_name";
    buf.dims[0] = 2;
    buf.dims[1] = 3;
    buf.dims[2] = 4;
    assert(H5Awrite(attr_id, compound_t, &buf ) >= 0);

    H5Aclose(attr_id);
	H5Tclose(compound_t);
    H5Sclose(space_id);
	H5Fclose(file);
}



