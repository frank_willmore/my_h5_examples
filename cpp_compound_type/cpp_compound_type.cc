#include <H5Cpp.h>

#include <iostream>
#include <vector>

using namespace H5;
using namespace std;

class frank_t {

    public:

        int    i;
        double d;

        frank_t(int _i, double _d){ i = _i; d = _d; }

} ;

int main(){

    vector<frank_t> my_frank; 
    
    for (int i=0;i<5;i++)  my_frank.push_back(frank_t(i, i+2.5));

    {
        hsize_t dim[] = {5};
        DataSpace space(1, dim);
        CompType mtype(sizeof(frank_t)); 
        mtype.insertMember("i_part", HOFFSET(frank_t, i), PredType::NATIVE_INT); 
        mtype.insertMember("d_part", HOFFSET(frank_t, d), PredType::NATIVE_DOUBLE); 
        H5File file("ftw.h5", H5F_ACC_TRUNC);
        DataSet dataset(file.createDataSet("my_data", mtype, space));
        dataset.write(&my_frank[0], mtype); 
    }

    cout << "now to read..." << endl;

    {
        H5File file("ftw.h5", H5F_ACC_RDONLY);
        DataSet dataset(file.openDataSet("my_data"));
        CompType mtype2 (sizeof(double));
        mtype2.insertMember("d_part", 0, PredType::NATIVE_DOUBLE); 
        double dd[5]; 
        dataset.read(dd, mtype2); 

        for (int i=0; i<5; i++) cout << "got back " << dd[i] << "." << endl;
    }
    
}
