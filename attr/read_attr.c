// read

#include <hdf5.h> 
#include <stdio.h> 
#include <assert.h>

void main(int argc, char *argv[]){

    hid_t fcpl_id = H5P_DEFAULT; 
    hid_t fapl_id = H5P_DEFAULT;
    hid_t file = H5Fopen( "attr.h5", H5F_ACC_RDONLY, H5P_DEFAULT );


    hid_t type_id = H5T_NATIVE_INT;
    const hsize_t current_dims[] = {3};
    const hsize_t maximum_dims[] = {3};
    hid_t space_id = H5Screate_simple( 1, current_dims, maximum_dims );
    hid_t aapl_id = H5P_DEFAULT;
    hid_t lapl_id = H5P_DEFAULT;

    int buf[3];
    hid_t attr_id = H5Aopen_by_name( file, "/", "my_attr", aapl_id, lapl_id );
    assert(attr_id >= 0);
    assert( H5Aread(attr_id, type_id, buf ) >= 0);

    int i;
    for (i=0;i<3;i++) printf("%d\n", buf[i]);
}

