#include <hdf5.h> 
#include <stdio.h> 
#include <assert.h>

void main(int argc, char *argv[]){

    hid_t fcpl_id = H5P_DEFAULT; 
    hid_t fapl_id = H5P_DEFAULT;
    hid_t file = H5Fcreate( "attr.h5", H5F_ACC_TRUNC, fcpl_id, fapl_id );


    hid_t type_id = H5T_NATIVE_INT;
    const hsize_t current_dims[] = {3};
    const hsize_t maximum_dims[] = {3};
    hid_t space_id = H5Screate_simple( 1, current_dims, maximum_dims );
    hid_t acpl_id = H5P_DEFAULT;
    hid_t aapl_id = H5P_DEFAULT;

    hid_t attr_id = H5Acreate2( file, "my_attr", type_id, space_id, acpl_id, aapl_id );
    hid_t mem_type_id = H5T_NATIVE_INT;
    const int buf[] = {12,34,56};
    assert(H5Awrite(attr_id, mem_type_id, buf ) >= 0);

}

