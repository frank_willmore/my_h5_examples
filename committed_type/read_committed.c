#include <hdf5.h>
#include <assert.h>
#include <stdio.h>

int main(int argc, char *argv[]){

    hid_t fapl_id = H5P_DEFAULT;
    hid_t file = H5Fopen("ct.h5", H5F_ACC_RDONLY, fapl_id);
    assert(file >= 0);
    
    //hid_t dtype_id = H5Tcopy(H5T_NATIVE_INT);

    //assert( H5Tcommit1( file, "my_cool_type", dtype_id ) >= 0 );
    hid_t tapl_id = H5P_DEFAULT;
    hid_t dtype_id = H5Topen(file, "my_cool_type", tapl_id);

    size_t size = H5Tget_size (dtype_id);

    printf("size = %d\n", size);

    H5Tclose(dtype_id);
    H5Fclose(file);

}
