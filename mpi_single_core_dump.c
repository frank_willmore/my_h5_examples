#include <mpi.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <stdio.h>

int main(int argc, char **argv){

    MPI_Init(&argc, &argv);

    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    int resource = RLIMIT_CORE;
    struct rlimit rlim;

    // gett current limits
    getrlimit(resource, &rlim);
    printf("Current(soft) / max(hard) limits for task %d:  %lu / %lu\n", rank, rlim.rlim_cur, rlim.rlim_max);

    if (rank != 2)
    {
        // setting vals to 0 will suppress core dump for all other ranks
        rlim.rlim_cur = 0;
        rlim.rlim_max = 0;
        setrlimit(resource, &rlim);
    }
    else
    {
        // setting vals to 0 will suppress core dump for all other ranks
        rlim.rlim_cur = 18446744073709551615u;
        rlim.rlim_max = 18446744073709551615u;
        setrlimit(resource, &rlim);
    }

    MPI_Barrier(MPI_COMM_WORLD);

    // get current limits
    getrlimit(resource, &rlim);
    printf("new (soft) / max(hard) limits for task %d:  %lu / %lu\n", rank, rlim.rlim_cur, rlim.rlim_max);

    // now fail
    int* p_int = 0;
    *p_int = 1;

    MPI_Finalize();

    return 0;
}