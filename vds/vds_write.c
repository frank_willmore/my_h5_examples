
#include "hdf5.h"
#include <stdio.h>
#include <stdlib.h>

#define FILE         "vds.h5"
#define DATASET      "VDS"
#define VDSDIM1         6 
#define VDSDIM0         4 
#define DIM0            6 
#define RANK1           1
#define RANK2           2

const char* SRC_FILE[] = { "a.h5", "b.h5", "c.h5" };
const char* SRC_DATASET[] = { "A", "B", "C" };

int main (void) {
    hid_t        subfile_id, vds_file_id, space, src_space, vspace, dset; /* Handles */ 
    hid_t        dcpl;
    herr_t       status;
    hsize_t      vdsdims[2] = {VDSDIM0, VDSDIM1},      /* Virtual datasets dimension */
                 source_dims[1] = {DIM0},                     /* Source datasets dimensions */
                 start[2],                             /* Hyperslab parameters */
                 stride[2],
                 count[2],
                 block[2];
    hsize_t      start_out[2],
                 stride_out[2],
                 count_out[2],
                 block_out[2];
    int          wdata[DIM0],                /* Write buffer for source dataset */
                 rdata[VDSDIM0][VDSDIM1],    /* Read buffer for virtual dataset */
                 i, j, k, l;  
    int          fill_value = -1;            /* Fill value for VDS */
    H5D_layout_t layout;                     /* Storage layout */
    size_t       num_map;                    /* Number of mappings */
    ssize_t      len;                        /* Length of the string; also a return value */
    char         *filename;                  
    char         *dsetname;
    hsize_t      nblocks;
    hsize_t      *buf;                       /* Buffer to hold hyperslab coordinates */

    for (i=0; i < 3; i++) { /* * Create source files and datasets. This step is optional.  */
        for (j = 0; j < DIM0; j++) wdata[j] = i+1; /* * Initialize data for i-th source dataset.  */
        
        /* * Create the source files and  datasets. Write data to each dataset and * close all resources.  */
        subfile_id = H5Fcreate (SRC_FILE[i], H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
        space = H5Screate_simple (RANK1, source_dims, NULL);
        dset = H5Dcreate (subfile_id, SRC_DATASET[i], H5T_NATIVE_INT, space, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Dwrite (dset, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, wdata);
        status = H5Sclose (space);
        status = H5Dclose (dset);
        status = H5Fclose (subfile_id);
    }
    status = H5Fclose (subfile_id);    

    /* Create file in which virtual dataset will be stored. */
    vds_file_id = H5Fcreate (FILE, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

    /* Create VDS dataspace.  */
    space = H5Screate_simple (RANK2, vdsdims, NULL);

    /* Set VDS creation property. */
    dcpl = H5Pcreate (H5P_DATASET_CREATE);
    status = H5Pset_fill_value (dcpl, H5T_NATIVE_INT, &fill_value);
     
    /* Initialize hyperslab values. */
    start[0] = 0;
    start[1] = 0;
    count[0] = 1;
    count[1] = 1;
    block[0] = 1;
    block[1] = VDSDIM1;

    /* * Build the mappings.  * Selections in the source datasets are H5S_ALL.  * In the virtual dataset we select the first, the second and the third rows * and map each row to the data in the corresponding source dataset.  */
    src_space = H5Screate_simple (RANK1, source_dims, NULL);
    for (i = 0; i < 3; i++) {
        start[0] = (hsize_t)i;
        /* Select i-th row in the virtual dataset; selection in the source datasets is the same. */
        status = H5Sselect_hyperslab (space, H5S_SELECT_SET, start, NULL, count, block);
        status = H5Pset_virtual (dcpl, space, SRC_FILE[i], SRC_DATASET[i], src_space);
    }

    /* Create a virtual dataset. */
    dset = H5Dcreate (vds_file_id, DATASET, H5T_NATIVE_INT, space, H5P_DEFAULT, dcpl, H5P_DEFAULT);
    status = H5Sclose (space);
    status = H5Sclose (src_space);
    status = H5Dclose (dset);
    status = H5Fclose (vds_file_id);    
     
    return 0;
}

