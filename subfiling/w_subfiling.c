#include <hdf5.h>
#include <stdlib.h>
#include <assert.h>
#include <stdio.h>

int main(int argc, char **argv) {

    int mpi_size, mpi_rank;				/* mpi variables */
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);

    hid_t fapl_id = H5Pcreate (H5P_FILE_ACCESS);
    H5Pset_fapl_mpio(fapl_id, MPI_COMM_WORLD, MPI_INFO_NULL);

    const char *filename = "Subfile_main.h5";
    char subfile_name[50];
    const unsigned n_dims = 2, n_rows = 3, n_cols = 4;
    hsize_t dims[] = {mpi_size * n_rows, n_cols};   	/* dataset dim sizes */
    hsize_t start[] = {mpi_rank * n_rows, 0};
    hsize_t count[] = {1,1};
    hsize_t stride[] = {1,1};
    hsize_t block[] = {n_rows, n_cols};

    /* set name of subfile */
    sprintf(subfile_name, "Subfile_%d.h5", mpi_rank);
    printf("rank %d has subfile_file name %s\n", mpi_rank, subfile_name);

    /* set number of process groups to be equal to the mpi size */
    assert(H5Pset_subfiling_access(fapl_id, subfile_name, MPI_COMM_SELF, MPI_INFO_NULL) >= 0);

    /* create the file. This should also create the subfiles */
    hid_t file_id = H5Fcreate(filename, H5F_ACC_TRUNC, H5P_DEFAULT, fapl_id);

    H5Pclose(fapl_id);

    /* create the dataspace for the master dataset */
    hid_t file_space_id = H5Screate_simple (n_dims, dims, NULL);

    /* set the selection for this dataset that this process will write to */
    assert(H5Sselect_hyperslab(file_space_id, H5S_SELECT_SET, start, stride, count, block) >= 0);

    /* create the dataset access property list */
    hid_t dapl_id = H5Pcreate(H5P_DATASET_ACCESS);

    /* Set the selection that this process will access the dataset with */
    assert(H5Pset_subfiling_selection(dapl_id, file_space_id) >= 0);

    /* create the dataset with the subfiling dapl settings */
    hid_t dataset_id = H5Dcreate2(file_id, "the_dataset", H5T_NATIVE_INT, file_space_id, H5P_DEFAULT, H5P_DEFAULT, dapl_id);

    H5Pclose(dapl_id);

    hsize_t npoints = n_rows * n_cols;
    unsigned* buf = (unsigned *)malloc(n_rows * n_cols * sizeof(unsigned));

    for(unsigned i=0 ; i<n_rows * n_cols; i++) buf[i] = (mpi_rank+1) * 10;

    hid_t mem_space_id = H5Screate_simple(1, &npoints, NULL);

    assert(H5Dwrite(dataset_id, H5T_NATIVE_INT, mem_space_id, file_space_id, H5P_DEFAULT, buf) >= 0);

    H5Dclose(dataset_id);

    H5Sclose(file_space_id);

    H5Sclose(mem_space_id);

    H5Fclose(file_id);

    free(buf);

    MPI_Finalize();
    return 0;

}

