#include <hdf5.h>
#include <stdlib.h>
#include <assert.h>
#include <stdio.h>

#define PATH_MAX 256
#define NFILENAME 2
#define PARATESTFILE filenames[0]
const char *FILENAME[NFILENAME]={ "SubFileTest", NULL};
char	filenames[NFILENAME][PATH_MAX];

const int split_way = 2;

#define DATASET1      "Dataset1_subfiled"
#define RANK          2
#define DIMS0         6
#define DIMS1         120

int main(int argc, char **argv) {

    int mpi_size, mpi_rank;				/* mpi variables */
    MPI_Init(&argc, &argv);
    MPI_Comm comm = MPI_COMM_WORLD;
    MPI_Info info = MPI_INFO_NULL;
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);

    /* setup file access property list */
    hid_t fapl_id = H5Pcreate (H5P_FILE_ACCESS);
    H5Pset_fapl_mpio(fapl_id, MPI_COMM_WORLD, MPI_INFO_NULL);

    hid_t fid;                  /* HDF5 file ID */
    hid_t did;
    hid_t sid, mem_space_id, dxpl_id;

    const char *filename = "SubFileTest.h5";
    char subfile_name[50];
    hsize_t npoints, start[RANK], count[RANK], stride[RANK], block[RANK];

    int mrc, color, i;

    int *rbuf;
    herr_t ret;         	/* Generic return value */

    /* create file access property list */
    fapl_id = H5Pcreate(H5P_FILE_ACCESS);
    ret = H5Pset_fapl_mpio(fapl_id, comm, info);

    /* set subfiling to be to 2 files */

    /* set name of subfile */
    sprintf(subfile_name, "Subfile_%d.h5", mpi_rank%2);

    /* splits processes into 2 groups */
    color = mpi_rank % split_way;
    mrc = MPI_Comm_split (MPI_COMM_WORLD, color, mpi_rank, &comm);

    /* set number of process groups to 2 */
    ret = H5Pset_subfiling_access(fapl_id, subfile_name, comm, MPI_INFO_NULL);

    /* open the file */
    fid = H5Fopen(filename, H5F_ACC_RDONLY, fapl_id);
    ret = H5Pclose(fapl_id);

    /* open the dataset */
    did = H5Dopen2(fid, DATASET1, H5P_DEFAULT);

    /* create the dataspace for the master dataset */
    sid = H5Dget_space(did);

    start[0] = DIMS0 * mpi_rank;
    start[1] = 0;
    block[0] = DIMS0;
    block[1] = DIMS1;
    count[0] = 1;
    count[1] = 1;
    stride[0] = 1;
    stride[1] = 1;

    /* set the selection for this dataset that this process will read from */
    ret = H5Sselect_hyperslab(sid, H5S_SELECT_SET, start, stride, count, block);

    npoints = DIMS0 * DIMS1;
    rbuf = (int *)malloc(npoints * mpi_size * sizeof(int));
    mem_space_id = H5Screate_simple(1, &npoints, NULL);

    /* Create dataset transfer property list */
    dxpl_id = H5Pcreate(H5P_DATASET_XFER);

    ret = H5Pset_dxpl_mpio(dxpl_id, H5FD_MPIO_COLLECTIVE);

    ret = H5Dread(did, H5T_NATIVE_INT, mem_space_id, sid, dxpl_id, rbuf);
//    if(ret < 0) FAIL_STACK_ERROR;

    {
        H5D_mpio_actual_io_mode_t io_mode;
        uint32_t local_cause, global_cause;

        ret = H5Pget_mpio_actual_io_mode(dxpl_id, &io_mode);
//        VRFY((ret == 0), "");
//        VRFY((io_mode == H5D_MPIO_CONTIGUOUS_COLLECTIVE), "Collective Mode invoked");

        H5Pget_mpio_no_collective_cause(dxpl_id, &local_cause, &global_cause);
//        VRFY((ret == 0), "");
//        VRFY((local_cause == H5D_MPIO_COLLECTIVE), "Collective I/O invoked");
//        VRFY((global_cause == H5D_MPIO_COLLECTIVE), "Collective I/O invoked");
    }

    ret = H5Pclose(dxpl_id);

//    for(i=0 ; i<npoints ; i++)
//        VRFY((rbuf[i] == (mpi_rank+1) * 10), "Data read verified");

    /* check if a process tries to access a different sub-file */
    start[0] = DIMS0 * ((mpi_rank+1) % mpi_size);
    start[1] = 0;
    block[0] = DIMS0;
    block[1] = DIMS1;
    count[0] = 1;
    count[1] = 1;
    stride[0] = 1;
    stride[1] = 1;
    /* set the selection for this dataset that this process will read from */
    ret = H5Sselect_hyperslab(sid, H5S_SELECT_SET, start, stride, count, block);
//    VRFY((ret == 0), "H5Sset_hyperslab succeeded");

    H5E_BEGIN_TRY {
        ret = H5Dread(did, H5T_NATIVE_INT, mem_space_id, sid, dxpl_id, rbuf);
    } H5E_END_TRY;
//    VRFY((ret < 0), "H5Dwrite failed");

    ret = H5Dclose(did);
//    VRFY((ret == 0), "");

    ret = H5Sclose(sid);
//    VRFY((ret == 0), "");

    ret = H5Fclose(fid);
//    VRFY((ret == 0), "");


    /* try to read with no subfiling on the opened file */

    /* create file access property list */
    fapl_id = H5Pcreate(H5P_FILE_ACCESS);
//    VRFY((fapl_id >= 0), "");
    ret = H5Pset_fapl_mpio(fapl_id, comm, info);
//    VRFY((ret == 0), "");
    fid = H5Fopen(filename, H5F_ACC_RDONLY, fapl_id);
//    VRFY((fid >= 0), "H5Fopen succeeded");
    ret = H5Pclose(fapl_id);
//    VRFY((ret == 0), "");

    /* open the dataset */
    did = H5Dopen2(fid, DATASET1, H5P_DEFAULT);
//    VRFY((did >= 0), "");

    /* Create dataset transfer property list */
    dxpl_id = H5Pcreate(H5P_DATASET_XFER);
//    VRFY((dxpl_id > 0), "H5Pcreate succeeded");

    /* set collective I/O which should be broken */
    ret = H5Pset_dxpl_mpio(dxpl_id, H5FD_MPIO_COLLECTIVE);
//    VRFY((ret >= 0), "H5Pset_dxpl_mpio succeeded");

    {
        hsize_t temp = npoints * mpi_size;

        mem_space_id = H5Screate_simple(1, &temp, NULL);
//        VRFY((mem_space_id >= 0), "");
    }

    ret = H5Dread(did, H5T_NATIVE_INT, mem_space_id, H5S_ALL, dxpl_id, rbuf);
//    if(ret < 0) FAIL_STACK_ERROR;
//    VRFY((ret == 0), "");

    {
        uint32_t local_cause, global_cause;

        H5Pget_mpio_no_collective_cause(dxpl_id, &local_cause, &global_cause);
//        VRFY((ret == 0), "");
//        VRFY((local_cause == H5D_MPIO_VDS_PARALLEL_READ), "Broke Collective I/O");
//        VRFY((global_cause == H5D_MPIO_VDS_PARALLEL_READ), "Broke Collective I/O");
    }

    ret = H5Pclose(dxpl_id);
//    VRFY((ret == 0), "");

    for(i=0 ; i<npoints*mpi_size ; i++) {
        int temp = i/npoints;

//        VRFY((rbuf[i] == (temp%mpi_size + 1) * 10), "Data read verified");
    }

    ret = H5Dclose(did);
//    VRFY((ret == 0), "");

    ret = H5Fclose(fid);
//    VRFY((ret == 0), "");

    ret = H5Sclose(mem_space_id);
//    VRFY((ret == 0), "");

    free(rbuf);

    mrc = MPI_Comm_free(&comm);
//    VRFY((mrc==MPI_SUCCESS), "MPI_Comm_free");

//    MPI_File_delete(subfile_name, MPI_INFO_NULL);
    MPI_Finalize();

    return 0;

}
