#include <hdf5.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define DIMS0 3
#define DIMS1 4

int main(int argc, char **argv){

    MPI_Comm comm = MPI_COMM_WORLD;
    MPI_Info info = MPI_INFO_NULL;

    MPI_Init(&argc, &argv);

    int mpi_size, mpi_rank;

    /* Get the # of processes in the communicator */
    MPI_Comm_size(MPI_COMM_WORLD,&mpi_size);

    /* Get the rank of the calling process in the communicator */
    MPI_Comm_rank(MPI_COMM_WORLD,&mpi_rank);

    /* Create file access property list */
    hid_t fapl_id = H5Pcreate(H5P_FILE_ACCESS);

    /* Set up MPI communicator info in the file access property list */
    H5Pset_fapl_mpio(fapl_id, comm, info);

    /* Set the name of the sub-file for the calling process */
    char subfile_name[256];
    sprintf(subfile_name, "Subfile_%d.h5", mpi_rank);

    printf("subfile: %s\n", subfile_name);


    /* Enable sub-filing in the file access property list */
    assert(H5Pset_subfiling_access(fapl_id, subfile_name, MPI_COMM_SELF, MPI_INFO_NULL)>=0);

    /* Create the file with the sub-filing feature. */
    /* This should create the master file and the sub-files */
    hid_t fid = H5Fcreate(subfile_name, H5F_ACC_TRUNC, H5P_DEFAULT, fapl_id);
    assert(fid >= 0);

    /* Create the dataspace for the master dataset */
    hsize_t dims[] = {DIMS0 * mpi_size, DIMS1};
    //hsize_t dims[1] = DIMS1;
    hid_t sid = H5Screate_simple (2, dims, NULL);


    H5Fclose(fid);
    H5Sclose(sid);
    H5Pclose(fapl_id);


    MPI_Finalize();

    return 0;
}
