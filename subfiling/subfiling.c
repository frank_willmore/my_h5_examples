#include <hdf5.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

void sub_write();
void sub_read();
void nosub_read();

const hsize_t n_dims = 2;
const hsize_t n_rows = 6, n_cols = 20;
char subfile_name[256];
const int split_way = 3;
MPI_Comm comm;

int main(int argc, char** argv){

    int mpi_size, mpi_rank;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);

    sprintf(subfile_name, "Subfile_%d.h5", mpi_rank);
//    printf("using subfile %s\n", subfile_name);

    sub_write(mpi_size, mpi_rank);
    sub_read(mpi_size, mpi_rank);
    nosub_read(mpi_size, mpi_rank);
    
    MPI_Finalize();
    return 0;
}

void nosub_read(int mpi_size, int mpi_rank){

    /* create file access property list */
    hid_t fapl_id = H5Pcreate(H5P_FILE_ACCESS);
    assert(H5Pset_fapl_mpio(fapl_id , MPI_COMM_WORLD, MPI_INFO_NULL) >= 0);
    hid_t file_id = H5Fopen("Subfile_main.h5", H5F_ACC_RDONLY, fapl_id );
    assert (H5Pclose(fapl_id ) >= 0);

    /* open the dataset */
    hid_t dset_id = H5Dopen(file_id, "the_dataset", H5P_DEFAULT);

    /* Create dataset transfer property list */
    hid_t dxpl_id = H5Pcreate(H5P_DATASET_XFER);

    /* set collective I/O which should be broken */
    assert (H5Pset_dxpl_mpio(dxpl_id, H5FD_MPIO_COLLECTIVE) >= 0);
//    VRFY((ret >= 0), "H5Pset_dxpl_mpio succeeded");

    hsize_t temp = n_rows * n_cols * mpi_size;
    hid_t mem_space_id = H5Screate_simple(1, &temp, NULL);

    int* rbuf = (int*)malloc(sizeof(int) * temp);
    assert(H5Dread(dset_id, H5T_NATIVE_INT, mem_space_id, H5S_ALL, dxpl_id, rbuf) >= 0);
    for (unsigned i=0; i<temp; i++) printf("%d", rbuf[i]);
    printf("\n\n");
//    uint32_t local_cause, global_cause;
//    H5Pget_mpio_no_collective_cause(dxpl_id, &local_cause, &global_cause);
    //VRFY((local_cause == H5D_MPIO_VDS_PARALLEL_READ), "Broke Collective I/O");
    //VRFY((global_cause == H5D_MPIO_VDS_PARALLEL_READ), "Broke Collective I/O");

    assert (H5Pclose(dxpl_id) >= 0);

//    for(int i=0 ; i<npoints*mpi_size ; i++) {
//        int temp = i/npoints;
//        VRFY((rbuf[i] == (temp%mpi_size + 1) * 10), "Data read verified");
//    }
    H5Dclose(dset_id);
    H5Fclose(file_id);
    H5Sclose(mem_space_id);
    free(rbuf);
}

void sub_read(int mpi_size, int mpi_rank){

    hsize_t dims[]   = {n_rows * mpi_size, n_cols};
    hsize_t start[]  = {n_rows * mpi_rank, 0};
    hsize_t count[]  = {1, 1};
    hsize_t stride[] = {1, 1};
    hsize_t block[]  = {n_rows, n_cols};

    hid_t fapl_id = H5Pcreate(H5P_FILE_ACCESS);
    assert(fapl_id >= 0);
    assert(H5Pset_fapl_mpio(fapl_id, MPI_COMM_WORLD, MPI_INFO_NULL) >= 0);


    // split by color
    int color = mpi_rank % split_way;
    MPI_Comm_split (MPI_COMM_WORLD, color, mpi_rank, &comm);
    sprintf(subfile_name, "Subfile_%d.h5", color);


    H5Pset_subfiling_access(fapl_id, subfile_name, comm, MPI_INFO_NULL);
    hid_t file_id = H5Fopen("Subfile_main.h5", H5F_ACC_RDONLY, fapl_id);
    H5Pclose(fapl_id);

    hid_t dset_id = H5Dopen(file_id, "the_dataset", H5P_DEFAULT); /* open the dataset */ 
    hid_t fspace_id = H5Dget_space(dset_id); /* create the dataspace for the master dataset */
    assert (H5Sselect_hyperslab(fspace_id, H5S_SELECT_SET, start, stride, count, block) >= 0);

    hsize_t npoints = n_rows * n_cols;
    unsigned* rbuf = (unsigned *)malloc(npoints * mpi_size * sizeof(unsigned));

    hid_t mem_space_id = H5Screate_simple(1, &npoints, NULL);

    assert (H5Dread(dset_id, H5T_NATIVE_INT, mem_space_id, fspace_id, H5P_DEFAULT, rbuf) >= 0);

    for (int i=0; i<npoints; i++) printf("%d\n", rbuf[i]);

    H5Dclose(dset_id);
    H5Sclose(fspace_id);
    H5Sclose(mem_space_id);
    H5Fclose(file_id);
}

void sub_write(int mpi_size, int mpi_rank){

    hsize_t dims[]   = {n_rows * mpi_size, n_cols};
    hsize_t start[]  = {n_rows * mpi_rank, 0};
    hsize_t count[]  = {1, 1};
    hsize_t stride[] = {1, 1};
    hsize_t block[]  = {n_rows, n_cols};

    hid_t fapl_id = H5Pcreate(H5P_FILE_ACCESS);
    assert(fapl_id >= 0);
    assert(H5Pset_fapl_mpio(fapl_id, MPI_COMM_WORLD, MPI_INFO_NULL) >= 0);


    // split by color
    int color = mpi_rank % split_way;
    MPI_Comm_split (MPI_COMM_WORLD, color, mpi_rank, &comm);
    sprintf(subfile_name, "Subfile_%d.h5", color);


    assert(H5Pset_subfiling_access(fapl_id, subfile_name, comm, MPI_INFO_NULL) >= 0);
    hid_t file_id = H5Fcreate("Subfile_main.h5", H5F_ACC_TRUNC, H5P_DEFAULT, fapl_id);
    assert(H5Pclose(fapl_id) >= 0);

    hid_t file_space_id = H5Screate_simple(n_dims, dims, NULL);
    hid_t mem_space_id  = H5Screate_simple(n_dims, block, NULL);

    assert (H5Sselect_hyperslab(file_space_id, H5S_SELECT_SET, start, stride, count, block) >= 0);
    hid_t dapl_id = H5Pcreate(H5P_DATASET_ACCESS);
    assert (H5Pset_subfiling_selection(dapl_id, file_space_id) >= 0);

    hid_t dataset_id = H5Dcreate(file_id, "the_dataset", H5T_NATIVE_INT, file_space_id, H5P_DEFAULT, H5P_DEFAULT, dapl_id);
    assert(H5Pclose(dapl_id) >= 0);
    
    unsigned* buf = (unsigned*)malloc(sizeof(unsigned)*n_rows*n_cols);
    for (unsigned i=0; i<n_rows * n_cols; i++) buf[i] = mpi_rank * n_rows * n_cols + i;

    // write the dang data
    assert (H5Dwrite(dataset_id, H5T_NATIVE_INT, mem_space_id, file_space_id, H5P_DEFAULT, buf) >= 0);

    H5Dclose(dataset_id);
    H5Sclose(file_space_id);
    H5Sclose(mem_space_id);
    H5Fclose(file_id);
    free(buf);
}


