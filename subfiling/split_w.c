#include <hdf5.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define DATASET1      "Dataset1_subfiled"
#define DIMS0         6
#define DIMS1         120

const int split_way = 2;

int main(int argc, char **argv)
{
    int mpi_size, mpi_rank;				/* mpi variables */

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);

    hid_t fid;                  /* HDF5 file ID */
    hid_t did;
    hid_t sid, mem_space_id;
    hid_t fapl_id, dapl_id, dxpl_id;	/* Property Lists */

    char subfile_name[50];
    hsize_t npoints;
    hsize_t dims[] = {DIMS0,DIMS1}, start[] = {DIMS0,0}, count[] = {1,1}, stride[] = {1,1}, block[] = {DIMS0,DIMS1};
    dims[0] *= mpi_size;
    start[0] *= mpi_rank;

    int mrc, color, i;
    MPI_Comm comm;
    MPI_Info info = MPI_INFO_NULL;

    int *wbuf;
    herr_t ret;         	/* Generic return value */

    const char *filename = "SubFileTest.h5";

    /* create file access property list */
    fapl_id = H5Pcreate(H5P_FILE_ACCESS);

    ret = H5Pset_fapl_mpio(fapl_id, MPI_COMM_WORLD, info);

    /* set subfiling to be to 2 files */
    color = mpi_rank % split_way;

    /* set name of subfile */
    sprintf(subfile_name, "Subfile_%d.h5", color);

    /* splits processes into 2 groups */
    mrc = MPI_Comm_split (MPI_COMM_WORLD, color, mpi_rank, &comm);
    assert (mrc == MPI_SUCCESS);

    /* set number of process groups to 2 */
    ret = H5Pset_subfiling_access(fapl_id, subfile_name, comm, MPI_INFO_NULL);

    /* create the file. This should also create the subfiles */
    fid = H5Fcreate(filename, H5F_ACC_TRUNC, H5P_DEFAULT, fapl_id);

    ret = H5Pclose(fapl_id);

    /* create the dataspace for the master dataset */
    sid = H5Screate_simple (2, dims, NULL);

    /* set the selection for this dataset that this process will write to */
    ret = H5Sselect_hyperslab(sid, H5S_SELECT_SET, start, stride, count, block);

    /* create the dataset access property list */
    dapl_id = H5Pcreate(H5P_DATASET_ACCESS);

    /* Set the selection that this process will access the dataset with */
    ret = H5Pset_subfiling_selection(dapl_id, sid);

    /* create the dataset with the subfiling dapl settings */
    did = H5Dcreate2(fid, DATASET1, H5T_NATIVE_INT, sid, H5P_DEFAULT, H5P_DEFAULT, dapl_id);

    ret = H5Pclose(dapl_id);

    npoints = DIMS0 * DIMS1;
    assert(npoints == H5Sget_select_npoints(sid));
    wbuf = (int *)malloc(npoints * sizeof(int));
    for(i=0 ; i<npoints ; i++) wbuf[i] = (mpi_rank+1) * 10;

    mem_space_id = H5Screate_simple(1, &npoints, NULL);

    /* Create dataset transfer property list */
    dxpl_id = H5Pcreate(H5P_DATASET_XFER);
    ret = H5Pset_dxpl_mpio(dxpl_id, H5FD_MPIO_COLLECTIVE);
    ret = H5Dwrite(did, H5T_NATIVE_INT, mem_space_id, sid, dxpl_id, wbuf);

    H5D_mpio_actual_io_mode_t io_mode;
    uint32_t local_cause, global_cause;

    ret = H5Pget_mpio_actual_io_mode(dxpl_id, &io_mode);
    H5Pget_mpio_no_collective_cause(dxpl_id, &local_cause, &global_cause);

    /* check if a process tries to access a different sub-file */
    start[0] = DIMS0 * ((mpi_rank+1) % mpi_size);
    start[1] = 0;
    block[0] = DIMS0;
    block[1] = DIMS1;
    count[0] = 1;
    count[1] = 1;
    stride[0] = 1;
    stride[1] = 1;

    /* set the selection for this dataset that this process will write to */
    ret = H5Sselect_hyperslab(sid, H5S_SELECT_SET, start, stride, count, block);

    /* Should fail sine we try to access wrong selection */
    H5E_BEGIN_TRY {
        ret = H5Dwrite(did, H5T_NATIVE_INT, mem_space_id, sid, dxpl_id, wbuf);
    } H5E_END_TRY;

    ret = H5Pclose(dxpl_id);
    ret = H5Dclose(did);
    ret = H5Sclose(sid);
    ret = H5Sclose(mem_space_id);
    ret = H5Fclose(fid);
    free(wbuf);

    mrc = MPI_Comm_free(&comm);

    MPI_Finalize();
    return 0;
}
