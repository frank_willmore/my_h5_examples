#define PARATESTFILE filenames[0]
#define PATH_MAX 256

#include <hdf5.h>
#include <stdlib.h>
#include <assert.h>

const char *FILENAME[2]={ "SubFileTest", NULL};
char	filenames[2][PATH_MAX];

#define DATASET1      "Dataset1_subfiled"
#define RANK          2
#define DIMS0         6
#define DIMS1         120

void subf_fpp_w(void); 

int main(int argc, char **argv)
{
    int mpi_size, mpi_rank;				/* mpi variables */

    MPI_Comm comm = MPI_COMM_WORLD;
    MPI_Info info = MPI_INFO_NULL;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);

    sprintf(filenames[0], "SubFileTest.h5");

    hid_t fapl_id = H5Pcreate (H5P_FILE_ACCESS);
    H5Pset_fapl_mpio(fapl_id, MPI_COMM_WORLD, MPI_INFO_NULL);

    // below is from test function

    hid_t file_id;                  /* HDF5 file ID */
    hid_t dataset_id;
    hid_t file_space_id, mem_space_id;
    hid_t dapl_id;	/* Property Lists */

    const char *filename;
    char subfile_name[50];
    hsize_t dims[RANK];   	/* dataset dim sizes */
    hsize_t npoints, start[RANK], count[RANK], stride[RANK], block[RANK];

    int *wbuf;              /* buffer for data */
    herr_t status;         	/* Generic return value */

    /* set name of subfile */
    sprintf(subfile_name, "Subfile_%d.h5", mpi_rank);
    printf("rank %d has subfile_file name %s\n", mpi_rank, subfile_name);

    /* set number of process groups to be equal to the mpi size */
    status = H5Pset_subfiling_access(fapl_id, subfile_name, MPI_COMM_SELF, MPI_INFO_NULL);

    /* create the file. This should also create the subfiles */
    file_id = H5Fcreate(filenames[0], H5F_ACC_TRUNC, H5P_DEFAULT, fapl_id);

    status = H5Pclose(fapl_id);

    dims[0] = DIMS0 * mpi_size;
    dims[1] = DIMS1;

    /* create the dataspace for the master dataset */
    file_space_id = H5Screate_simple (2, dims, NULL);

    start[0] = DIMS0 * mpi_rank;
    start[1] = 0;
    block[0] = DIMS0;
    block[1] = DIMS1;
    count[0] = 1;
    count[1] = 1;
    stride[0] = 1;
    stride[1] = 1;

    /* set the selection for this dataset that this process will write to */
    status = H5Sselect_hyperslab(file_space_id, H5S_SELECT_SET, start, stride, count, block);

    /* create the dataset access property list */
    dapl_id = H5Pcreate(H5P_DATASET_ACCESS);

    /* Set the selection that this process will access the dataset with */
    status = H5Pset_subfiling_selection(dapl_id, file_space_id);

    /* create the dataset with the subfiling dapl settings */
    dataset_id = H5Dcreate2(file_id, DATASET1, H5T_NATIVE_INT, file_space_id, H5P_DEFAULT, H5P_DEFAULT, dapl_id);

    status = H5Pclose(dapl_id);

    npoints = DIMS0 * DIMS1;
    wbuf = (int *)malloc(npoints * sizeof(int));

    for(unsigned i=0 ; i<npoints ; i++) wbuf[i] = (mpi_rank+1) * 10;

    assert(npoints == H5Sget_select_npoints(file_space_id));

    mem_space_id = H5Screate_simple(1, &npoints, NULL);

    status = H5Dwrite(dataset_id, H5T_NATIVE_INT, mem_space_id, file_space_id, H5P_DEFAULT, wbuf);

    status = H5Dclose(dataset_id);

    status = H5Sclose(file_space_id);

    status = H5Sclose(mem_space_id);

    status = H5Fclose(file_id);

    free(wbuf);

    MPI_Barrier(MPI_COMM_WORLD); /* make sure all processes are finished before final report, cleanup * and exit.  */

    MPI_Finalize();
    return 0;

}

