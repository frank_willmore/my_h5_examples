#include <hdf5.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define NFILENAME 2
#define PARATESTFILE filenames[0]
#define PATH_MAX 256

const char *FILENAME[NFILENAME]={ "Subfile_main", NULL};
char	filenames[NFILENAME][PATH_MAX];

#define DATASET1      "the_dataset"
#define RANK          2
#define DIMS0         3
#define DIMS1         4

int main(int argc, char **argv) {

    int mpi_size, mpi_rank;				/* mpi variables */

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
    MPI_Comm comm = MPI_COMM_WORLD;
    MPI_Info info = MPI_INFO_NULL;

    hid_t fapl = H5Pcreate (H5P_FILE_ACCESS);
    assert(fapl >= 0);
    herr_t ret = H5Pset_fapl_mpio(fapl, comm, info);
    assert(ret >= 0);

    hid_t fid;                  /* HDF5 file ID */
    hid_t did;
    hid_t sid, mem_space_id;
    hid_t dxpl_id;  	/* Property Lists */

    const char *filename = "Subfile_main.h5";
    char subfile_name[50];
    hsize_t npoints, start[RANK], count[RANK], stride[RANK], block[RANK];

    // buffer for read
    int *rbuf;

    /* set subfiling to be 1 file per mpi rank */

    /* set name of subfile */
    sprintf(subfile_name, "Subfile_%d.h5", mpi_rank);
printf("Subfile name: %s\n", subfile_name);

    /* set number of process groups to be equal to the mpi size */
    ret = H5Pset_subfiling_access(fapl, subfile_name, MPI_COMM_SELF, MPI_INFO_NULL);
    assert(ret>=0);

    /* open the file */
printf("filename=%s\n", filename);
    fid = H5Fopen(filename, H5F_ACC_RDONLY, fapl);
    assert(fid >= 0);

    ret = H5Pclose(fapl);

    /* open the dataset */
    did = H5Dopen2(fid, DATASET1, H5P_DEFAULT);

    /* create the dataspace for the master dataset */
    sid = H5Dget_space(did);

    start[0] = DIMS0 * mpi_rank;
    start[1] = 0;
    block[0] = DIMS0;
    block[1] = DIMS1;
    count[0] = 1;
    count[1] = 1;
    stride[0] = 1;
    stride[1] = 1;

    /* set the selection for this dataset that this process will read from */
    ret = H5Sselect_hyperslab(sid, H5S_SELECT_SET, start, stride, count, block);

    npoints = DIMS0 * DIMS1;
    rbuf = (int *)malloc(npoints * mpi_size * sizeof(int));

    mem_space_id = H5Screate_simple(1, &npoints, NULL);

    ret = H5Dread(did, H5T_NATIVE_INT, mem_space_id, sid, H5P_DEFAULT, rbuf);

//    for(i=0 ; i<npoints ; i++)
//        VRFY((rbuf[i] == (mpi_rank+1) * 10), "Data read verified");

    ret = H5Dclose(did);
    ret = H5Sclose(sid);
    ret = H5Sclose(mem_space_id);
    ret = H5Fclose(fid);

    /* try to read with no subfiling on the opened file */

    /* create file access property list */
    fapl= H5Pcreate(H5P_FILE_ACCESS);
    ret = H5Pset_fapl_mpio(fapl, comm, info);
    fid = H5Fopen(filename, H5F_ACC_RDONLY, fapl);
    ret = H5Pclose(fapl);

    /* open the dataset */
    did = H5Dopen2(fid, DATASET1, H5P_DEFAULT);

    /* Create dataset transfer property list */
    dxpl_id = H5Pcreate(H5P_DATASET_XFER);

    /* set collective I/O which should be broken */
    ret = H5Pset_dxpl_mpio(dxpl_id, H5FD_MPIO_COLLECTIVE);
//    VRFY((ret >= 0), "H5Pset_dxpl_mpio succeeded");

    hsize_t temp = npoints * mpi_size;
    mem_space_id = H5Screate_simple(1, &temp, NULL);

    ret = H5Dread(did, H5T_NATIVE_INT, mem_space_id, H5S_ALL, dxpl_id, rbuf);

    uint32_t local_cause, global_cause;
    H5Pget_mpio_no_collective_cause(dxpl_id, &local_cause, &global_cause);
    //VRFY((local_cause == H5D_MPIO_VDS_PARALLEL_READ), "Broke Collective I/O");
    //VRFY((global_cause == H5D_MPIO_VDS_PARALLEL_READ), "Broke Collective I/O");

    ret = H5Pclose(dxpl_id);

    for(int i=0 ; i<npoints*mpi_size ; i++) {
        int temp = i/npoints;

//        VRFY((rbuf[i] == (temp%mpi_size + 1) * 10), "Data read verified");
    }

    ret = H5Dclose(did);
    ret = H5Fclose(fid);
    ret = H5Sclose(mem_space_id);
    free(rbuf);

//    MPI_File_delete(subfile_name, MPI_INFO_NULL);

    MPI_Finalize();

    return 0;
}





