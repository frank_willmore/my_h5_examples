#include "mpi.h"
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#define TRUE 1

void main(int argc, char **argv){

    MPI_File fh;
    int rank, size;

    MPI_Init(&argc, &argv);

    MPI_File_open(MPI_COMM_WORLD, "file.wrk", MPI_MODE_RDWR | MPI_MODE_CREATE, MPI_INFO_NULL, &fh);

    MPI_File_set_view( fh, 0, MPI_INT, MPI_INT, "native", MPI_INFO_NULL ) ; 

//    MPI_File_set_atomicity( fh, TRUE ) ; 

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    MPI_Status status;
    int *data = (int *)malloc(1024*sizeof(MPI_INT));
    memset(data, rank, 1024*4 );

    MPI_File_write_at(fh, 1024*rank, data, 1024, MPI_INT, &status) ; 

    MPI_File_sync(fh);

    MPI_Finalize();


}
